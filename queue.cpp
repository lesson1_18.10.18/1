#include "queue.h"
#include <iostream>

using namespace std;

void initQueue(queue* q, unsigned int size)
{
	q->numberInTheQueue = new int[size];
	q->numOfElements = 0;
	q->size = size;
}

void cleanQueue(queue* q)
{
	delete(q->numberInTheQueue);
}

void enqueue(queue* q, unsigned int newValue)
{
	if (q->numOfElements < q->size)
	{
		q->numberInTheQueue[q->numOfElements] = newValue;
		q->numOfElements++;
	}
	else
	{
		std::cout << "The queue is full, cant add" << std::endl;
	}
}

int dequeue(queue* q)
{
	int i = 0;
	int top = q->numberInTheQueue[0];//the top element to return
	
	//Going over all to elemnts to move them one place back
	for (i = 0; i < (q->numOfElements - 1); i++)
	{
		q->numberInTheQueue[i] = q->numberInTheQueue[i + 1];
	}
	
	//if i = 0 it means there is only 1 element in the queue so i need to remove it wihtout placing any other value
	if (i == 0)
	{
		q->numberInTheQueue[0] = 0;
	}
	q->numOfElements--;//reduce amount of elements
	return top;
}