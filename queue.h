#pragma once
/* a queue contains positive integer values. */
typedef struct queue
{
	int* numberInTheQueue;
	int numOfElements;
	int size;
} queue;

void initQueue(queue* q, unsigned int size);
void cleanQueue(queue* q);

void enqueue(queue* q, unsigned int newValue);
int dequeue(queue* q); // return element in top of queue, or -1 if empty